package invoice

import (
	"encoding/json"
	"log"
	"net/http"
)

type Handler struct{}

func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("Invoice created!"))
}

func (h *Handler) FindById(w http.ResponseWriter, r *http.Request) {
	log.Println("Running a READ request...")
	invoice, exists := loadInvoices()[r.PathValue("id")]

	if !exists {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Item doesn't exist!"))
		return
	}

	json.NewEncoder(w).Encode(invoice)
}

func (h *Handler) UpdateById(w http.ResponseWriter, r *http.Request) {
	log.Println("Processing UPDATE request...", r.Method)
}

func (h *Handler) DeleteById(w http.ResponseWriter, r *http.Request) {
	log.Println("Processing DELETE request...")
}

func (h *Handler) PatchById(w http.ResponseWriter, r *http.Request) {
}

func (h *Handler) Options(w http.ResponseWriter, r *http.Request) {
}
