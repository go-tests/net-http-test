package main

import (
	"fmt"
	"net/http"

	"gitlab.com/go-tests/net-http-test/comment"
	invoice "gitlab.com/go-tests/net-http-test/invoices"
	"gitlab.com/go-tests/net-http-test/items"
	"gitlab.com/go-tests/net-http-test/middleware"
)

func main() {
	router := http.NewServeMux()
	adminRouter := http.NewServeMux()
	authorizedRouter := http.NewServeMux()

	itemHandler := &items.Handler{}
	invoiceHandler := &invoice.Handler{}
	commentHandler := &comment.Handler{}

	router.HandleFunc("GET /items", itemHandler.GetAll)
	router.HandleFunc("GET /items/{id}", itemHandler.FindById)
	router.HandleFunc("POST /items", itemHandler.Create)
	router.HandleFunc("PATCH /items/{id}", itemHandler.PatchById)
	router.HandleFunc("DELETE /items/{id}", itemHandler.DeleteById)

	middlewareStack := middleware.CreateStack(
		middleware.Logging,
	// add more middleware as needed
	)

	v1 := http.NewServeMux()
	v1.Handle("/v1/", http.StripPrefix("/v1", router)) // now every route is handled by v1
	server := http.Server{
		Addr:    "localhost:8080",
		Handler: middlewareStack(v1),
	}

	router.HandleFunc("GET /invoice/{id}", invoiceHandler.FindById)

	router.Handle("/", middleware.CheckAdmin(adminRouter))

	adminRouter.HandleFunc("POST /invoice", invoiceHandler.Create)
	adminRouter.HandleFunc("PATCH /invoice/{id}", itemHandler.PatchById)
	adminRouter.HandleFunc("DELETE /invoice/{id}", itemHandler.DeleteById)

	router.Handle("/comments", middleware.IsAuthorized(authorizedRouter))

	authorizedRouter.HandleFunc("POST /comments", commentHandler.Create)

	fmt.Println("Server listening on port 8080...")
	server.ListenAndServe()
}
