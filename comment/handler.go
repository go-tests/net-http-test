package comment

import (
	"log"
	"net/http"

	"gitlab.com/go-tests/net-http-test/middleware"
)

type Handler struct{}

func NewHandler() *Handler {
	return &Handler{}
}

func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	userId, ok := r.Context().Value(middleware.AuthUserId).(string)
	if !ok {
		log.Println("Invalid userID!")
		w.WriteHeader(http.StatusBadRequest)
	}

	log.Println("Creating comment...")
	w.Write([]byte("Comment created for: " + userId))
}
