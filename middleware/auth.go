package middleware

import (
	"context"
	"encoding/base64"
	"net/http"
	"strings"
)

const AuthUserId string = "middleware.auth.userId"

func WriteUnauthed(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
	w.Write([]byte(http.StatusText(http.StatusUnauthorized)))
}

func IsAuthorized(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorization := r.Header.Get("Authorization")

		// Check that the header begins with a prefix of Bearer
		if !strings.HasPrefix(authorization, "Bearer ") {
			WriteUnauthed(w)
			return
		}

		// get the token
		encodedToken := strings.TrimPrefix(authorization, "Bearer ")
		// Decode the token from base 64
		token, err := base64.StdEncoding.DecodeString(encodedToken)
		if err != nil {
			WriteUnauthed(w)
			return
		}

		// We're just assuming a valid base64 token is a valid user id
		userID := string(token)

		ctx := context.WithValue(r.Context(), AuthUserId, userID)
		req := r.WithContext(ctx)

		h.ServeHTTP(w, req)
	})
}
