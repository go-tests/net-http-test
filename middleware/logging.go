package middleware

import (
	"log"
	"net/http"
	"time"
)

type Writer struct {
	http.ResponseWriter
	statuscode int16
}

func (w *Writer) WriteHeader(s int) {

}

func Logging(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		startTime := time.Now()

		writer := &Writer{
			ResponseWriter: w,
			statuscode:     http.StatusOK,
		}

		h.ServeHTTP(w, r)
		log.Println(writer.statuscode, r.URL.Path, r.URL.Hostname(), time.Since(startTime))
	})
}
