package middleware

import "net/http"

type Middleware func(http.Handler) http.Handler

func CreateStack(stack ...Middleware) Middleware {
	return func(h http.Handler) http.Handler {
		for i := len(stack) - 1; i >= 0; i-- {
			currentMiddleware := stack[i]
			h = currentMiddleware(h)
		}
		return h
	}
}
