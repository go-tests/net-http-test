package items

import (
	"encoding/json"
	"log"
	"net/http"
)

type Handler struct{}

func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusCreated)
	log.Println("Created a request...")
	w.Write([]byte("Request handled!"))
}

func (h *Handler) GetAll(w http.ResponseWriter, r *http.Request) {
	log.Println("Running a READ request...")

	items := loadItems()

	json.NewEncoder(w).Encode(items)
}

func (h *Handler) FindById(w http.ResponseWriter, r *http.Request) {
	log.Println("Running a READ request...")
	item, exists := loadItems()[r.PathValue("id")]

	if !exists {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("Item doesn't exist!"))
		return
	}

	json.NewEncoder(w).Encode(item)
}

func (h *Handler) UpdateById(w http.ResponseWriter, r *http.Request) {
	log.Println("Processing UPDATE request...", r.Method)
}

func (h *Handler) DeleteById(w http.ResponseWriter, r *http.Request) {
	log.Println("Processing DELETE request...")
}

func (h *Handler) PatchById(w http.ResponseWriter, r *http.Request) {
}

func (h *Handler) Options(w http.ResponseWriter, r *http.Request) {
}
