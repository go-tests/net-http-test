package items

import "strconv"

type Items struct {
	ID          uint     `json: "id"`
	Name        string   `json: "name"`
	Description string   `json: "description"`
	Features    []string `json: "features"`
}

func itemList() []Items {
	return []Items{
		{
			ID:          1,
			Name:        "Product 1",
			Description: "This is a great product that will change your life.",
			Features: []string{
				"Feature 1",
				"Feature 2",
				"Feature 3",
			},
		},
		{
			ID:          2,
			Name:        "Product 2",
			Description: "This is another great product, but even better!",
			Features: []string{
				"Feature A",
				"Feature B",
				"Feature C",
			},
		},
		{
			ID:          3,
			Name:        "Product 3",
			Description: "You won't regret buying this amazing product.",
			Features: []string{
				"Feature X",
				"Feature Y",
				"Feature Z",
			},
		},
		{
			ID:          4,
			Name:        "Productivity WIDget",
			Description: "Boost your efficiency with this handy productivity tool.",
			Features: []string{
				"Task management",
				"Time tracking",
				"Goal setting",
			},
		},
		{
			ID:          5,
			Name:        "Wireless Headphones",
			Description: "Enjoy crystal clear sound without any wires holding you back.",
			Features: []string{
				"Active noise cancellation",
				"Long battery life",
				"Comfortable fit",
			},
		},
	}
}

func loadItems() map[string]Items {
	itemsList := itemList()
	res := make(map[string]Items, len(itemsList))

	for _, item := range itemsList {
		res[strconv.Itoa(int(item.ID))] = item
	}

	return res
}
